# Microsoft Teams App Requirements

## SCRIPT

[- Reference](https://docs.microsoft.com/en-us/microsoftteams/platform/get-started/prerequisites?tabs=cli)

- Node Js (v14 LTS release)

- Web Browser

- Visual Studio Code

- Install the [teams toolkit](https://login.microsoftonline.com/common/oauth2/authorize?response_type=code&client_id=aebc6443-996d-45c2-90f0-388ff96faa56&redirect_uri=https%3A%2F%2Fvscode-redirect.azurewebsites.net%2F&state=45135,M3p204JzdLxwfJld4FP6EQ%3D%3D&resource=https%3A%2F%2Fmanagement.core.windows.net%2F&prompt=select_account)

- Install [Asure Functions Core Tools](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local?tabs=linux%2Ccsharp%2Cbash)

- Install [.NET SDK](https://docs.microsoft.com/en-us/dotnet/core/install/linux-ubuntu#2004-)

- Install [ngrok](https://ngrok.com/download)

- Select something on Teams

- Google Chrome [React Developper Tools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi)

- [Graph Explorer](https://developer.microsoft.com/fr-fr/graph/graph-explorer)

- [MS Dashborad](https://dev.teams.microsoft.com/apps)

## LINK

[MS_APP_REQUIREMENTS](https://web.microsoftstream.com/video/27166375-adb3-4fdf-82cc-24ff3ea6e65e)

---

<div align="center">

<a href="https://gitlab.com/blacky-yg" target="_blank"><img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/gitlab.svg" alt="gitlab.com" width="30"></a>

</div>