# Pull/Merge requests

## How to

- checkout
- commit
- create
- review
- approve
- merge

- Pull request template

---

<div align="center">

<a href="https://gitlab.com/blacky-yg" target="_blank"><img src="https://cdn.jsdelivr.net/npm/simple-icons@3.0.1/icons/gitlab.svg" alt="gitlab.com" width="30"></a>

</div>